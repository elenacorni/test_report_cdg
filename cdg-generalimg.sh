#!/bin/bash

return_help=0

if ! which pdfimages > /dev/null; then
	return_help=1
fi
if ! which pdftotext > /dev/null; then
	return_help=1
fi
if ! which montage > /dev/null; then
	return_help=1
fi

the_help(){
	echo -e "\nUsage: $0 [--help or -h]"
	echo -e "\n\n----\nThis script use 'pdfimages' and 'pdftotext' ('poppler-utils' package) and 'montage' ('imagemagick' package)."
	echo "They can be installed with:"
	echo -e "\n(sudo apt-get update)"
	echo "sudo apt-get install poppler-utils (or yum install poppler-utils)"
	echo -e "sudo apt-get install imagemagick\n" 
}

if [[ ( $1 == "--help" ) || ( $1 == "-h" ) || ( $return_help -eq 1 ) ]]
then
	the_help
	exit 1
fi

##################################

rm -rf pdf/ common_img/
mkdir pdf common_img

cd pdf

#WLCG
let k=0
while [ 1 ]
do
    YEAR=`date +%Y -d "$k month ago"`
    FOLD=`date +%m-%y -d "$k month ago"`
    MONTH_n=`date +%m -d "$k month ago"`

    M=(zero Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
    MONTH=${M[${MONTH_n}]}

    echo "$YEAR $FOLD $MONTH"

    for exp in ALICE ATLAS CMS LHCB
    do
	    wget https://espace.cern.ch/WLCG-document-repository/ReliabilityAvailability/${YEAR}/${FOLD}/WLCG_Tier1_VO_${exp}_${MONTH}${YEAR}.pdf

	    return_wget=${?}

	    if [ ${return_wget} -eq 0 ]
	    then
		    
	    	    # save all images from this pdf:
	    	    pdfimages -png WLCG_Tier1_VO_${exp}_${MONTH}${YEAR}.pdf -f 2 -l 2 ${exp}_${MONTH}${YEAR}_av
	    	    pdfimages -png WLCG_Tier1_VO_${exp}_${MONTH}${YEAR}.pdf -f 4 -l 4 ${exp}_${MONTH}${YEAR}_re

	    	    # save two tables:
	    	    pdftotext -layout WLCG_Tier1_VO_${exp}_${MONTH}${YEAR}.pdf -f 3 -l 3 ${exp}_${MONTH}${YEAR}_av_table
	    	    pdftotext -layout WLCG_Tier1_VO_${exp}_${MONTH}${YEAR}.pdf -f 5 -l 5 ${exp}_${MONTH}${YEAR}_re_table

		    cat ${exp}_${MONTH}${YEAR}_av_table | grep Average | sed '1d' >> ${exp}_${MONTH}${YEAR}_av_table_last && sed -i 's/\%//g' ${exp}_${MONTH}${YEAR}_av_table_last
		    cat ${exp}_${MONTH}${YEAR}_re_table | grep Average | sed '1d' >> ${exp}_${MONTH}${YEAR}_re_table_last && sed -i 's/\%//g' ${exp}_${MONTH}${YEAR}_re_table_last
	   fi
    done

    if [ ${return_wget} -eq 0 ]
    then
	    # create img:
	    montage ALICE_${MONTH}${YEAR}_av-003.png	ALICE_${MONTH}${YEAR}_re-003.png  -tile 1x2  -geometry +1+1 ../common_img/alice_tab.png
	    montage ATLAS_${MONTH}${YEAR}_av-005.png	ATLAS_${MONTH}${YEAR}_re-005.png  -tile 1x2  -geometry +1+1 ../common_img/atlas_tab.png
	    montage CMS_${MONTH}${YEAR}_av-005.png	CMS_${MONTH}${YEAR}_re-005.png	  -tile 1x2  -geometry +1+1 ../common_img/cms_tab.png
	    montage LHCB_${MONTH}${YEAR}_av-002.png	LHCB_${MONTH}${YEAR}_re-002.png   -tile 1x2  -geometry +1+1 ../common_img/lhcb_tab.png

	    for i in av re
	    do
	            # save values of tables into variables: 
		    alice_cnaf=$(awk '{ print $6}' ALICE_${MONTH}${YEAR}_${i}_table_last)
	            alice_aver=$(awk '{ print $22}' ALICE_${MONTH}${YEAR}_${i}_table_last)
	            atlas_cnaf=$(awk '{ print $10}' ATLAS_${MONTH}${YEAR}_${i}_table_last)
	            atlas_aver=$(awk '{ print $28}' ATLAS_${MONTH}${YEAR}_${i}_table_last)
	            cms_cnaf=$(awk '{ print $10}' CMS_${MONTH}${YEAR}_${i}_table_last)
	            cms_aver=$(awk '{ print $18}' CMS_${MONTH}${YEAR}_${i}_table_last)
	            lhcb_cnaf=$(awk '{ print $4}' LHCB_${MONTH}${YEAR}_${i}_table_last)
	            lhcb_aver=$(awk '{ print $18}' LHCB_${MONTH}${YEAR}_${i}_table_last)


		    # check if exist a same date and if not exist save new data and push two txt files on remote repository as backup in case of accidental deletation
		    test=( $(awk '{ print $1}' ../${i}_history.txt ) )
		    check=0
		    for j in "${test[@]}"
		    do
			    if [ "$j" == "$FOLD" ]
			    then
				    check=1
			    fi
		    done

		    if [ $check -eq 0 ]
		    then
			    # put all values into txt files:
			    echo -e "$FOLD\t$alice_cnaf\t$alice_aver\t$atlas_cnaf\t$atlas_aver\t$cms_cnaf\t$cms_aver\t$lhcb_cnaf\t$lhcb_aver" >> ../${i}_history.txt
			    git add ../${i}_history.txt
			    git commit -m $FOLD
			    git push origin master
		    fi
	    done
    fi

    if [ ${return_wget} -eq 0 ]
    then
	break
    else
        let k=k+1
    fi
done

cd ..
rm -rf pdf/



