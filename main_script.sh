#!/bin/bash

return_help=0

if [ ! -f file.key.pem -o ! -f file.crt.pem ] ; then
	return_help=1
fi
if ! which pdfimages > /dev/null; then
        return_help=1
fi
if ! which pdftotext > /dev/null; then
        return_help=1
fi
if ! which montage > /dev/null; then
        return_help=1
fi

the_help(){
	echo -e "\nUsage: $0 <start-date> <end-date>  [--help or -h]
	Where: [staert-date] and [and-date] are REQUIRED (format YYYY-mm-gg)"
	echo -e "\n\n Files Required in this folder: 'file.crt.pem', 'file.key.pem', to create them you can use:"
	echo -e "> openssl pkcs12 -in <file.p12> -out file.crt.pem -clcerts -nokeys"
	echo -e "> openssl pkcs12 -in <file.p12> -out file.key.pem -nocerts -nodes"
	echo -e "\n\n----\nThis script use 'pdfimages' and 'pdftotext' ('poppler-utils' package) and 'montage' ('imagemagick' package)."
	echo "They can be installed with:"
	echo -e "\n(> sudo apt-get update)"
	echo "> sudo apt-get install poppler-utils (or yum install poppler-utils)"
	echo "> sudo apt-get install imagemagick"
	echo -e "\n\n----\nCheck if your 'awk' version is at least 4.0 or majior using command:"
	echo "> awk -W version"
	echo "To upgrade:"
	echo -e "> sudo apt-get install gawk\n"
}

if [[ ( $1 == "--help" ) || ( $1 == "-h" ) || ( -z $1 ) || ( -z $2  ) || ( $return_help -eq 1 ) ]]
then
	the_help
	exit 1
fi



##############################################
start_date=$1
end_date=$2

git clone <REPOSITORY-BALTIG>
cd <REPOSITORY-BATLTIG>

# I SEZIONE: Utilizzo delle risore - Farming

### DA FARE!! ###

# III SEZIONE: Av&Re
./cdg-generalimg.sh
python plot.py

# IMMAGINI COMUNI:
python plots_farmPlotsUtil_and_grafana_disk.py ${start_date} ${end_date}
./common_img.sh

# IV TICKETS:
./ticket.sh ${start_date} ${end_date}

rm ...rrr

git add ...
git commit -m '...'
git push origin master #overleaf

echo 




