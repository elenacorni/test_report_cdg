import pylab as pl

tmp = open("av_history.txt", "r")
rows_av = (row.strip().split() for row in tmp)
col_av = zip(*rows_av)

tmp = open("re_history.txt", "r")
rows_re = (row.strip().split() for row in tmp)
col_re = zip(*rows_re)

# AVAILABILITY:
    
tot = len(col_av[0])
x = range(12)

# alice:
pl.figure(1)
pl.xticks(x, col_av[0][tot-12:tot], rotation=45)
pl.plot(x,col_av[1][tot-12:tot])
pl.plot(x,col_av[2][tot-12:tot])

pl.ylim([0,103])
pl.grid()
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('ALICE - Availability')
pl.show()
pl.savefig('Availability_Alice_Historical.png')

# atlas:
pl.figure(1)
pl.xticks(x, col_av[0][tot-12:tot], rotation=45)
pl.plot(x,col_av[3][tot-12:tot])
pl.plot(x,col_av[4][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('ATLAS - Availability')
pl.show()
pl.savefig('Availability_Atlas_Historical.png')

# cms:
pl.figure(1)
pl.xticks(x, col_av[0][tot-12:tot], rotation=45)
pl.plot(x,col_av[5][tot-12:tot])
pl.plot(x,col_av[6][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('CMS - Availability')
pl.show()
pl.savefig('Availability_Cms_Historical.png')

# lhcb:
pl.figure(1)
pl.xticks(x, col_av[0][tot-12:tot], rotation=45)
pl.plot(x,col_av[7][tot-12:tot])
pl.plot(x,col_av[8][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('LHCB - Availability')
pl.show()
pl.savefig('Availability_Lhcb_Historical.png')

# RELIABILITY:

# alice:
pl.figure(1)
pl.xticks(x, col_re[0][tot-12:tot], rotation=45)
pl.plot(x,col_re[1][tot-12:tot])
pl.plot(x,col_re[2][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('ALICE - Reliability')
pl.show()
pl.savefig('Reliability_Alice_Historical.png')

# atlas:
pl.figure(1)
pl.xticks(x, col_re[0][tot-12:tot], rotation=45)
pl.plot(x,col_re[3][tot-12:tot])
pl.plot(x,col_re[4][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('ATLAS - Reliability')
pl.show()
pl.savefig('Reliability_Atlas_Historical.png')

# cms:
pl.figure(1)
pl.xticks(x, col_re[0][tot-12:tot], rotation=45)
pl.plot(x,col_re[5][tot-12:tot])
pl.plot(x,col_re[6][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('CMS - Reliability')
pl.show()
pl.savefig('Reliability_Cms_Historical.png')

# lhcb:
pl.figure(1)
pl.xticks(x, col_re[0][tot-12:tot], rotation=45)
pl.plot(x,col_re[7][tot-12:tot])
pl.plot(x,col_re[8][tot-12:tot])

pl.grid()
pl.ylim([0,103])
pl.legend(['CNAF', 'Average'], loc='lower right')
pl.title('LHCB - Reliability')
pl.show()
pl.savefig('Reliability_Lhcb_Historical.png')


