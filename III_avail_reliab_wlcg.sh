start_date=$1
end_date=$2

yyyyS=`date -d $start_date +%Y`
yyyyE=`date -d $end_date +%Y`

#mmS=`date -d $start_date +%m`
#mmE=`date -d $end_date +%m`

echo "start: $start_date ---> year: $yyyyS; m: $mmS"
echo "end:   $end_date ---> year: $yyyyE; m: $mmE"

diff_y=$((yyyyE-yyyyS))
M=(zero Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)

mkdir pdf
cd pdf

if [ $diff_y -eq 0 ]
then
	yy=`date -d $start_date +%y`
	for (( mese=$mmS; mese<=$mmE; c++ ))
	do
		string_m=${M[${mese}]}
		for exp in ALICE ATLAS CMS LHCB
		do
			wget https://espace.cern.ch/WLCG-document-repository/ReliabilityAvailability/${yyyyS}/${mese}-${yy}/WLCG_All_Sites_${exp}_${string_m}${yyyyS}.pdf
		done
	done
fi

cd ..


#diff_m=$((mmE-mmS))



